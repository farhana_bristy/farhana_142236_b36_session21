<?php
namespace App\Model;        //App means src/BITM/SEIP142236

use PDO;
use PDOException;

class Database
{
    public $DBH;
    public $host = 'localhost';
    public $dbname = 'atomic_project_b36';
    public $user ='root';
    public $password = '';
    public function __construct()
    {
        try
        {
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password);
            echo 'Hurrah!!!Database connected successfully';
            echo '<br>';
        }
        catch(PDOException $error)
        {
            echo $error->getMessage();
        }
    }
}